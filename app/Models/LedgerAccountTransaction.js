'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class LedgerAccountTransaction extends Model {
  static get hidden(){
    return ['id','updated_at']
  }
}

module.exports = LedgerAccountTransaction
