'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class WalletContact extends Model {
  static get hidden(){
    return ['id','created_at','user_id','updated_at']
  }
}

module.exports = WalletContact
