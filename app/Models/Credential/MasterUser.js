'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const {encrypt, decrypt,hash} = use('App/Helper/EncryptionHelper')

class MasterUser extends Model {
  static boot() {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await hash(userInstance.password)
      }
      if (userInstance.dirty.mnemonic) {
        userInstance.mnemonic = await encrypt(userInstance.mnemonic)
      }
      if (userInstance.dirty.privateKey) {
        userInstance.privateKey = await encrypt(userInstance.privateKey)
      }
      if (userInstance.dirty.publicKey) {
        userInstance.publicKey = await encrypt(userInstance.publicKey)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/WalletAuthToken')
  }

  wallet() {
    return this.belongsTo('App/Models/WalletEthereum', 'id', 'userId')
  }

  privilege(){
    return this.hasMany('App/Models/Credential/SystemUserPrivilege','id','user_id')
  }

  static get hidden(){
    return ['created_at','updated_at','id','privilege_group_id','password']
  }
}

module.exports = MasterUser
