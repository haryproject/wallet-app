'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PrivilegeEcosystem extends Model {
  ecosystem(){
    return this.belongsTo('App/Models/Credential/MasterEcosystem','ecosystemId','id')
  }
  static get hidden(){
    return ['created_at','updated_at','id','privilegeGroupId']
  }
}


module.exports = PrivilegeEcosystem
