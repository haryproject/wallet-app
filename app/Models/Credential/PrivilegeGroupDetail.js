'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PrivilegeGroupDetail extends Model {

  privilege(){
    return this.belongsTo('App/Models/System/Privilege','privilege_id','id')
  }

  static get hidden(){
    return ['created_at','updated_at','id','privilegeGroupId']
  }
}

module.exports = PrivilegeGroupDetail
