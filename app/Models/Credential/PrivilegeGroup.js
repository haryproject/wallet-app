'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PrivilegeGroup extends Model {

  ecosystem(){
    return this.hasMany('App/Models/Credential/PrivilegeEcosystem','id','privilegeGroupId')
  }
  privileges(){
    return this.hasMany('App/Models/Credential/PrivilegeGroupDetail','id','privilegeGroupId')
  }

  static get hidden(){
    return ['created_at','updated_at','id']
  }
}

module.exports = PrivilegeGroup
