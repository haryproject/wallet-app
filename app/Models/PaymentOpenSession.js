'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PaymentOpenSession extends Model {
  static get hidden(){
    return ['id','created_at']
  }
}

module.exports = PaymentOpenSession
