'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class WalletFeeStructure extends Model {
  static get hidden(){
    return ['updated_at','id']
  }
}

module.exports = WalletFeeStructure
