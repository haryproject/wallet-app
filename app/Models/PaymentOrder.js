'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PaymentOrder extends Model {

  merchant(){
    return this.belongsTo('App/Models/PaymentMerchant','merchant_id','id')
  }

  static get hidden(){
    return ['updated_at','id','merchant_id']
  }
}

module.exports = PaymentOrder
