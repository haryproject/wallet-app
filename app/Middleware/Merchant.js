'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const CredentialChecker = use('App/Helper/CredentialChecker')
const {Ecosystem, Grade, Action} = use('App/Helper/MasterCredential')

class Merchant {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request ,response,auth}, next) {
    // call next to advance the request
    const user = auth.user
    const isCredentialCorrect = await CredentialChecker.properUserChecker(user,
      Ecosystem.PAYMENT,
      [
        Grade.MERCHANT
      ],
      [
        Action.CREATE_ORDER,
        Action.CHECK_CLIENT_BALANCE
      ])
    if (!isCredentialCorrect) return response.forbidden({message: "Not Auhtorized"})
    await next()
  }


}

module.exports = Merchant
