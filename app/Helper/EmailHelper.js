const axios = require('axios')

const Env = use('Env')
const apiKey = Env.getOrFail('APP_KEY')
const mailServer = Env.getOrFail('MAIL_SERVER')


module.exports.sendDepositConfirmation = async function (token, tokenSymbol, total, tax, fiatSymbol, link, receiver, subject) {
  let result;

  await axios.post(mailServer + '/api/v1/mail/' + apiKey + '/confirm/deposit', {
    token,
    tokenSymbol,
    total,
    tax,
    fiatSymbol,
    link,
    receiver,
    subject
  })
    .then(function (response) {
      result = response.data
      return response.data
    })
    .catch(function (error) {

      return false
    });
  // console.log(result)
  return result
}

module.exports.sendDepositSuccessConfirmation = async function (token, tokenSymbol, total, tax, fiatSymbol, receiver, subject) {
  let result;

  await axios.post(mailServer + '/api/v1/mail/' + apiKey + '/deposit/success', {
    token,
    tokenSymbol,
    total,
    tax,
    fiatSymbol,
    receiver,
    subject
  })
    .then(function (response) {
      result = response.data
      return response.data
    })
    .catch(function (error) {

      return false
    });
  // console.log(result)
  return result
}

module.exports.sendForgotPinConfirmation = async function (link, receiver, subject) {
  let result;

  await axios.post(mailServer + '/api/v1/mail/' + apiKey + '/forgot/pin', {
    link,
    receiver,
    subject
  })
    .then(function (response) {
      result = response.data
      return response.data
    })
    .catch(function (error) {

      return false
    });
  // console.log(result)
  return result
}

module.exports.sendPaymentSuccessConfirmation = async function (merchant, orderId, tokenSymbol, amount, tax, receiver, subject) {
  let result;

  await axios.post(mailServer + '/api/v1/mail/' + apiKey + '/payment/success', {
    merchant,
    orderId,
    tokenSymbol,
    amount,
    tax,
    receiver,
    subject
  })
    .then(function (response) {
      result = response.data
      return response.data
    })
    .catch(function (error) {

      return false
    });
  // console.log(result)
  return result
}

module.exports.sendPaymentFailedConfirmation = async function (merchant, orderId, tokenSymbol, amount, tax, message, receiver, subject) {
  let result;

  await axios.post(mailServer + '/api/v1/mail/' + apiKey + '/payment/failed', {
    merchant,
    orderId,
    tokenSymbol,
    amount,
    tax,
    message,
    receiver,
    subject
  })
    .then(function (response) {
      result = response.data
      return response.data
    })
    .catch(function (error) {

      return false
    });
  // console.log(result)
  return result
}

module.exports.sendTransferSuccessConfirmation = async function ( token, tokenSymbol,  tax, address, user,receiver, subject,) {
  let result;

  await axios.post(mailServer + '/api/v1/mail/' + apiKey + '/transfer/success', {
    token,
    tokenSymbol,
    tax,
    address,
    receiver,
    subject,
    user
  })
    .then(function (response) {
      result = response.data
      return response.data
    })
    .catch(function (error) {

      return false
    });
  // console.log(result)
  return result
}

module.exports.sendTransferFailedConfirmation = async function ( token, tokenSymbol, address,user, receiver, subject,) {
  let result;

  await axios.post(mailServer + '/api/v1/mail/' + apiKey + '/transfer/failed', {
    token,
    tokenSymbol,
    address,
    user,
    receiver,
    subject,
  })
    .then(function (response) {
      result = response.data
      return response.data
    })
    .catch(function (error) {
      return false
    });
  // console.log(result)
  return result
}





