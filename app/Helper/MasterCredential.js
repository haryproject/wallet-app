
const Ecosystem = {
  SYSTEM: 1,
  LISTING: 2,
  WALLET: 3,
  MARKET: 4,
  DSHARE: 5,
  PAYMENT:6,
  LEDGER:7
}

const Authority = {
  ADMIN_SU: 1,
  CLIENT_NV: 2,
  CLIENT_V: 3,
  LENDER:4,
  MERCHANT:5,
  SANDBOX_BOT:6
}

const Action = {
  LOGIN: 101,
  REGISTER: 102,
  LOG_ACCESS: 103,

  DEPOSIT: 201,
  WITHDRAW: 202,
  CONTROL: 203,
  GENERATE: 204,


  MODIFY_KYC: 301,
  SEE_KYC: 302,
  TRADE:304,

  APPROVE_KYC: 401,
  LOOKOP: 402,

  LIST_A_COIN: 501,

  CREATE_ORDER:601,
  CHECK_CLIENT_BALANCE:602,
}

module.exports = {Ecosystem,Authority,Action}
