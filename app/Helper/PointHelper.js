const Point = use('App/Models/WalletPoint')
const PointHelper = use('App/Models/WalletPointHistory')


module.exports.increase = async function (address, point, environment) {
  await PointHelper.create({address, point, side: 'deposit', environment})
  const userPoint = await Point.findBy({address, environment})
  if (userPoint) {
    await Point.query().where({address, environment}).increment('point', point)
    return await Point.findBy({address, environment})
  }
  return await Point.create({address, point, environment})
}

module.exports.decrease = async function (address, point, environment) {
  await PointHelper.create({address, point, side: 'withdraw', environment})
  const userPoint = await Point.findBy({address, environment})
  if (userPoint) {
    await Point.query().where({address, environment}).decrement('point',point)
    return await Point.findBy({address, environment})
  }
  return {point:0}
}
