const Web3 = require('web3');
const Web3HDWalletProvider = require("web3-hdwallet-provider");

const fs = require('fs');
const rawdata = fs.readFileSync('app/Helper/FixedToken.json');

const ContractRaw = JSON.parse(rawdata);
const ABI = ContractRaw['abi']



module.exports.ethConnector = function (url,mnemonic) {
  const ethHttpProvider = new Web3.providers.HttpProvider(url);
  const ethProvider = new Web3HDWalletProvider(mnemonic, ethHttpProvider);
  const ethConnector = new Web3(Web3.givenProvider || ethProvider);
  return ethConnector
}

module.exports.contractConnector = function (url,mnemonic, contract_address, gas_price) {
  const contractTttpProvider = new Web3.providers.HttpProvider(url);
  const contractProvider = new Web3HDWalletProvider(mnemonic, contractTttpProvider);
  const contractWeb3 = new Web3(Web3.givenProvider || contractProvider);
  const contractConnector = new contractWeb3.eth.Contract(ABI, contract_address, {
    gasPrice: gas_price.toString(),
  })
  return contractConnector
}


module.exports.ethConnectorLocal = function (url,mnemonic) {
  // console.log({uuid, receiver, amount,environment,configId})
  const ethHttpProviderLocal = new Web3.providers.HttpProvider(url);
  const ethProviderLocal = new Web3HDWalletProvider(mnemonic, ethHttpProviderLocal);
  const ethConnectorLocal = new Web3(Web3.givenProvider || ethProviderLocal);
  return ethConnectorLocal
}

module.exports.contractConnectorLocal = function (url,mnemonic, contract_address, gas_price) {
  // console.log({url,mnemonic, contract_address, gas_price})
  const cotractHttpProviderLocal = new Web3.providers.HttpProvider(url);
  const contractProviderLocal = new Web3HDWalletProvider(mnemonic, cotractHttpProviderLocal);
  const contractWeb3Local = new Web3(Web3.givenProvider || contractProviderLocal);
  const contractConnectorLocal = new contractWeb3Local.eth.Contract(ABI, contract_address, {
    gasPrice: gas_price.toString(),
  })
  return contractConnectorLocal
}








module.exports.socketConnector = function (contract_address) {
  const socketProvider = new Web3.providers.WebsocketProvider(ETHEREUM_NETWORK_SOCKET)
  const web3socket = new Web3(Web3.givenProvider || socketProvider);
  const contractSocket = new web3socket.eth.Contract(ABI, contract_address);
  return {web3socket, contractSocket}
}

module.exports.contractHistory = function (contract_address) {
  const web3socket = new Web3(Web3.givenProvider || httpProvider);
  const contractSocket = new web3socket.eth.Contract(ABI, contract_address);
  return {web3socket, contractSocket}
}

module.exports.STX = "stx"
