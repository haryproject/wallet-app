const Database = use('Database')


module.exports.getUserHistory = async function (user_id, year, month, date) {

  if (!year && !month && !date) {
    return await Database
      .raw("select " +
        "wallet_transfers.to,transaction_number,source,amount,cleanAmount,taxAmount,note,transactionHash," +
        "created_at " +
        "from wallet_transfers " +
        "where user_id='" + user_id + "'" +
        "order by created_at desc");
  } else if (!month && !date) {
    return await Database
      .raw("select " +
        "wallet_transfers.to,transaction_number,source,amount,cleanAmount,taxAmount,note,transactionHash," +
        "created_at " +
        "from wallet_transfers " +
        "where user_id='" + user_id + "' " +
        "and year(created_at)= " + year + " " +
        "order by created_at desc");
  } else if (!date) {
    return await Database
      .raw("select " +
        "wallet_transfers.to,transaction_number,source,amount,cleanAmount,taxAmount,note,transactionHash," +
        "created_at " +
        "from wallet_transfers " +
        "where user_id='" + user_id + "' " +
        "and year(created_at)= " + year + " " +
        "and month(created_at) = " + month + " " +
        "order by created_at desc ");

  } else {
    return await Database
      .raw("select " +
        "wallet_transfers.to,transaction_number,source,amount,cleanAmount,taxAmount,note,transactionHash," +
        "created_at " +
        "from wallet_transfers " +
        "where user_id='" + user_id + "' " +
        "and year(created_at)= " + year + " " +
        "and month(created_at) = " + month + " " +
        "and day(created_at) = " + date + " " +
        "order by created_at desc ");

  }


}
