'use strict'

class SendForgotPinEmail {
  get rules () {
    return {
      link:'required',
      uniqueKey:'required'
    }
  }

  get mmessages(){
    return {
      'link.required':'Link is required',
      'uniqueKey.required':'Unique Key is required'
    }
  }
}

module.exports = SendForgotPinEmail
