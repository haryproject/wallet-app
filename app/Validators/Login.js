'use strict'

class Login {
  get rules () {
    return {
     'email':"required|email",
      "password":"required"
    }
  }
  get messages(){
    return {
      'email.required':"Email must not empty",
      'email.email':"Email format is not correct",
      'password.required':"Password must not empty"
    }
  }
}

module.exports = Login
