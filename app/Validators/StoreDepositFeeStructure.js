'use strict'

class StoreDepositFeeStructure {
  get rules() {
    return {
      threshold: 'required|number',
      amount: 'required|number',
      percentage: 'required|number',

    }
  }

  get messages(){
    return {
      'threshold.required':'Threshold is must be defined',
      'amount.required': 'Amount is must be defined',
      'percentage.required': 'Percentage is must be defined',
      'threshold.number': 'Threshold is must be a number',
      'amount.number': 'Amount is must be a number',
      'percentage.number': 'Percentage is must be a number',
    }
  }
}

module.exports = StoreDepositFeeStructure
