'use strict'

class Deposit {
  get rules() {
    return {
      fiatAmount: 'required',
      return_url: 'required',
      cancel_url: 'required'
    }
  }

  get messages() {
    return {
      'fiatAmount.required': 'Amount is required',
      'return_url.required': 'Success return URL',
      'cancel_url.required': 'Cancel return URL'
    }
  }
}

module.exports = Deposit
