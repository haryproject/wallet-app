'use strict'

class Transfer {
  get rules () {
    return {
      amount:"required|number",
      to:"required"
    }
  }


  get messeges(){
    return {
      'amount.required':'This field is required'
    }
  }
}

module.exports = Transfer
