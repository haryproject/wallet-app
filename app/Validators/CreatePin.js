'use strict'

class ChangePin {
  get rules () {
    return {
      pin:'required|min:6|max:6'
    }
  }

  get messages(){
    return {
      'pin.required':'Pin is required',
      'pin.min':"Minimum pin's length is 6",
      'pin.max':"Maximum pin's length is 6",
    }
  }
}

module.exports = ChangePin
