'use strict'

class OverviewSendToken {
  get rules () {
    return {
      to:'required',
      amount:'required|number'

    }
  }
  get messages () {
    return {
      'to.required':'Receiver address must not be empty',
      'amount.required':'Amount is required',
      'amount.number':'Amount must be a number'

    }
  }
}

module.exports = OverviewSendToken
