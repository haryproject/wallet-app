'use strict'

class SendToken {
  get rules () {
    return {
      to:'required',
      amount:'required|number',
    }
  }
  get messages () {
    return {
      'to.required':'Receiver address must not be empty',
      'amount.required':'Clean amount is required',
      'amount.number':'Clean amount must be a number',

    }
  }
}

module.exports = SendToken
