'use strict'

class WalletConfig {
  get rules () {
    return {
      'name':'required',
      'symbol':'required',
      'decimals':'required',
      'local_address':'required',
      'live_address':'required',
      'lender_uuid':'required',
      'lender_address':'required',
      'live_network_url':'required',
      'live_websocket_url':'required',
      'local_network_url':'required',
      'local_websocket_url':'required',
    }
  }

  get messages () {
    return {
      'name.required':'Name is required',
      'symbol.required':'Symbol is required',
      'decimals.required':'Decimals is required',
      'live_address.required':'Live contract address is required',
      'local_address.required':'Local contract address is required',
      'lender_uuid.required':'Lender uuid is required',
      'lender_address.required':"Lender Address is required",
      'live_network_url.required':'Live Network URL is required',
      'live_websocket_url.required':'Live Websocket URL is required',
      'local_network_url.required':'Local Network URL is required',
      'local_websocket_url.required':'Local Websocket URL is required',
    }
  }
}

module.exports = WalletConfig
