'use strict'

class StoreContact {
  get rules () {
    return {
      name:"required",
      address:'required'
    }
  }

  get messages(){
    return{
      'name.required':'Name must not be empty',
      'address.required':'Address must not be empty'
    }
  }
}

module.exports = StoreContact
