'use strict'

class FeeStructure {
  get rules() {
    return {
      amount: 'required',
      tax: 'required',
      point_commision: 'required',
      gasprice: "required"
    }
  }

  get messages() {
    return {
      'amount.required': 'This field is required',
      'tax.required': 'This field is required',
      'point_commision.required': 'This field is required',
      'gasprice.required': "This field is required"
    }
  }
}

module.exports = FeeStructure
