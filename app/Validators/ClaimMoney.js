'use strict'

class ClaimMoney {
  get rules () {
    return {
     'order':'required'
    }
  }

  get messages(){
    return {
      'order.required':'Order ID is required'
    }
  }
}

module.exports = ClaimMoney
