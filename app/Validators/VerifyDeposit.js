'use strict'

class VerifyDeposit {
  get rules () {
    return {
      paymentId:'required',
      PayerID:'required',
    }
  }

  get messages(){
    return {
      'paymentId.required':'Payment Id is Required',
      'PayerID.required':'Payer Id is Required'
    }
  }
}

module.exports = VerifyDeposit
