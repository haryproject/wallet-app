'use strict'

class UserRegister {
  get rules () {
    return {
      'username':'required|unique,master_users:username',
      'password':'required',
      'email':'required|email|unique,master_users:email'
    }
  }

  get messages(){
    return {
      'username.required': 'Must not be empty',
      'password.required': 'Must not be empty',
      'email.required': 'Must not be empty',
      'username.unique': 'Already exist',
      'email.unique': 'Already exist',
      'email.email':'Email format not correct'
    }
  }
}

module.exports = UserRegister
