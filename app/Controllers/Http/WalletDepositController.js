'use strict'
const FeeStructure = use('App/Models/WalletDepositFeeStructure')
const WalletConfiguration = use('App/Models/WalletConfig')
const paypal = require('paypal-rest-sdk');
const DepositHistory = use('App/Models/WalletDepositOrder')
const randomNumber = require('random-number');
const randomNumberOptions = {min: 100000, max: 999999, integer: true}
const {sendDepositConfirmation, sendDepositSuccessConfirmation} = use('App/Helper/EmailHelper')
const Promise = require('promise');
const EthereumWallet = use('App/Models/WalletEthereum')
const {lendEth, giveToken} = require('starx-lender-client')
const Transaction = use('App/Models/LedgerAccountTransaction')
const Env = use('Env')
const uuid = require('uuid/v4')
const Account = use('App/Models/Credential/MasterUser')
const StarxConverter = require('starx-conveter')

class WalletDepositController {

  async preview({request, response, auth, params}) {
    const {amount} = request.all()
    const fee = await FeeStructure.query().where('amount', '<=', amount).orderBy('amount', 'desc').first()
    if (!fee) return response.badRequest({message: "Please define the fee structure"})

    const totalAmount = parseFloat(amount) + (fee.amount + (amount * (fee.percentage / 100)))
    const starx = StarxConverter.fromFiat({usdAmount: amount})
    return {amount, totalAmount, starx}
  }

  async deposit({request, response, auth, params}) {
    const {fiatAmount, return_url, cancel_url} = request.all()
    console.log(fiatAmount, return_url, cancel_url)
    const token = await WalletConfiguration.findBy({type: 'token', mandatory: '1'})
    if (!token) return response.notFound({message: "Token not found"})

    const {paypal_client_id, paypal_secret} = token

    const fee = await FeeStructure.query().where('amount', '<=', fiatAmount).orderBy('amount', 'desc').first()
    if (!fee) return response.badRequest({message: "Please define the fee structure"})

    const totalAmount = parseFloat(fiatAmount)
    // Add Converter Here
    const tokenAmount = StarxConverter.fromFiat({usdAmount: totalAmount})
    const totalFiatPayment = (parseFloat(totalAmount) + (fee.amount + (totalAmount * fee.percentage)))
    const tokenPrice = (parseFloat(totalFiatPayment) / parseFloat(tokenAmount)).toFixed(2).toString()

    paypal.configure({
      'mode': 'sandbox', //sandbox or live
      'client_id': paypal_client_id,
      'client_secret': paypal_secret
    });

    // Build PayPal order request
    // var payReq = JSON.stringify({
    //   intent: 'authorize',
    //   payer: {
    //     payment_method: 'paypal'
    //   },
    //   redirect_urls: {
    //     return_url: return_url,
    //     cancel_url: cancel_url
    //   },
    //   transactions: [{
    //     amount: {
    //       details: {
    //         subtotal: totalFiatPayment.toString(),
    //         tax: 0
    //       },
    //       currency: "USD",
    //       total: totalFiatPayment.toString()
    //     },
    //     description: 'This is the payment transaction description.',
    //     invoice_number: Math.random() * 10000,
    //     payment_options: {
    //       allowed_payment_method: 'INSTANT_FUNDING_SOURCE'
    //     },
    //     item_list: {
    //       items: [{
    //         name: "Deposit for " + tokenAmount + " STX",
    //         sku: "DP_STX",
    //         price: totalFiatPayment.toString(),
    //         currency: "USD",
    //         quantity: "1"
    //       }]
    //     }
    //   }]
    // });

    var payReq = JSON.stringify({
      intent: 'sale',
      payer: {
        payment_method: 'paypal'
      },
      redirect_urls: {
        return_url: return_url,
        cancel_url: cancel_url
      },
      transactions: [{
        amount: {
          currency: "USD",
          total: totalFiatPayment.toLocaleString('fullwide', { useGrouping: false }),
        },
        description: 'This is the payment transaction description.',
      }]
    });

    console.log(payReq)

    const createPaymentLink = new Promise((resolve, reject) => {
      paypal.payment.create(payReq, async (error, callback) => {
        if (error) {
          reject(error)
          return error;

        } else {
          const randomCode = await randomNumber(randomNumberOptions)
          console.log(await callback)
          resolve(callback.links[1].href.toString())


          await DepositHistory.create({
            paypal_order_id: callback.id,
            user_id: auth.user.id,
            amount: fiatAmount,
            token: tokenAmount,
            tax: totalFiatPayment.toLocaleString('fullwide', { useGrouping: false }),
            payment_link: callback.links[1].href.toString(),
            status: 'pending'
          })

        }
      });

    })


    let paymentLink;
    await createPaymentLink.then(x => paymentLink = x)
    return {
      totalUSD: totalFiatPayment,
      token: tokenAmount,
      paymentLink
    }

  }

  async confirmDeposit({request, response, auth, params}) {
    const {environment} = params
    const {paymentId, PayerID} = request.all()

    const token = await WalletConfiguration.findBy({type: 'token', mandatory: '1'})
    if (!token) return response.notFound({message: "Token not found"})

    const {paypal_client_id, paypal_secret} = token

    const depositOrder = await DepositHistory.findBy({paypal_order_id: paymentId})
    if (!depositOrder) return {}

    const account = await Account.find(depositOrder.user_id)

    const userWallet = await EthereumWallet.findBy({userId: depositOrder.user_id})


    var payerId = {payer_id: PayerID};

    const confirmation = new Promise((resolve, reject) => {
      paypal.payment.execute(paymentId, payerId, async function(error, payment) {
        if (error) {
          reject(error)
        } else {
          resolve(payment)
          if (payment.state == 'approved') {
            depositOrder.status = 'authorized'
            // await depositOrder.save()


            const sendEmailReceipt = await sendDepositSuccessConfirmation(
              depositOrder.token,
              token.symbol,
              depositOrder.amount,
              depositOrder.tax,
              "USD",
              account.email,
              "Deposit Success"
            )

            console.log(sendEmailReceipt)

            const transaction_unique = uuid()
            const ledgerTransfer = await Transaction.create({
              transaction_no: transaction_unique,
              sender: "system",
              receiver: userWallet.address,
              amount: depositOrder.token.toLocaleString('fullwide', { useGrouping: false }),
              source: 'token',
              type: 'deposit'
            })

            console.log({deposit: ledgerTransfer.toJSON()})


          } else {

          }
        }
      });


    })

    await confirmation
    return {message: "Success"}
  }

  async confirmDepositSaved({request, response, auth, params}) {
    const {environment} = params
    const {paymentId, PayerID} = request.all()

    const token = await WalletConfiguration.findBy({type: 'token', mandatory: '1'})
    if (!token) return response.notFound({message: "Token not found"})

    const {paypal_client_id, paypal_secret} = token

    const depositOrder = await DepositHistory.findBy({paypal_order_id: paymentId})
    if (!depositOrder) return {}

    const account = await Account.find(depositOrder.user_id)

    const userWallet = await EthereumWallet.findBy({userId: depositOrder.user_id})
    const payerId = {
      payer_id: PayerID,
      transactions: [{
        amount: {
          currency: "USD",
          total: depositOrder.tax.toString()
        },
        description: "Token Deposit"
      }]
    };

    paypal.configure({
      'mode': 'sandbox', //sandbox or live
      'client_id': paypal_client_id,
      'client_secret': paypal_secret
    });

    const orderPromiseId = new Promise((resolve, reject) => {
      paypal.payment.execute(paymentId, payerId, async function (error, payment) {
        if (error) {
          reject(error)
        } else {
          if (payment.state === 'approved'
            && payment.transactions
            && payment.transactions[0].related_resources
            && payment.transactions[0].related_resources[0].order) {

            // Capture order id
            const order = payment.transactions[0].related_resources[0].order.id;
            depositOrder.order_id = order
            depositOrder.status = 'authorized'
            await depositOrder.save()
            resolve(order)

          } else {
            return {message: 'no redirect URI present'}
          }
        }
      });

    })

    const orderId = await orderPromiseId

    const capture_details = {
      amount: {
        currency: 'USD',
        total: depositOrder.tax.toString(),
      },
    };

    const authorizeDepositPromise = new Promise((resolve, reject) => {
      paypal.order.authorize(orderId, capture_details, function (error, authorization) {
        if (error) {
          reject(error)
        } else {
          capture_details.is_final_capture = true
          paypal.order.capture(orderId, capture_details, async function (error, capture) {
            if (error) {
              reject(error)
            } else {
              resolve(capture)
              depositOrder.order_id = orderId
              depositOrder.status = 'captured'
              await depositOrder.save()

              const config = {
                host: Env.getOrFail('ETH_SERVER'),
                uuid: token.lender_uuid,
                receiver: userWallet.address,
                amount: depositOrder.token.toLocaleString('fullwide', { useGrouping: false }),
                environment,
                configId: token.id
              }

              // Give token to user
              const giveTokenTransaction = await giveToken(config)
              console.log(giveTokenTransaction)

              const sendEmailReceipt = await sendDepositSuccessConfirmation(
                depositOrder.token,
                token.symbol,
                depositOrder.amount,
                depositOrder.tax,
                "USD",
                account.email,
                "Deposit Success"
              )

              console.log(sendEmailReceipt)

              const transaction_unique = uuid()
              const ledgerTransfer = await Transaction.create({
                transaction_no: transaction_unique,
                sender: "system",
                receiver: userWallet.address,
                amount: depositOrder.token,
                source: 'token',
                type: 'deposit'
              })

              console.log({deposit: ledgerTransfer.toJSON()})


            }
          });
        }
      });
    })


    return {
      message: "Success",
      authorize: await authorizeDepositPromise
    }
  }

}

module.exports = WalletDepositController
