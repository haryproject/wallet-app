'use strict'

const Coins = use('App/Models/ListingToken')
const PaymentConfig = use('App/Models/PaymentConfig')

const {contractConnector, contractConnectorLocal, STX, ethConnector} = use('App/Helper/EthereumHelper')

const User = use('App/Models/Credential/MasterUser')
const Wallet = use('App/Models/MasterEthereumWallet')
const UserPrivilege = use('App/Models/Credential/SystemUserPrivilege')
const BigNumber = require('bignumber.js');
const {encrypt, decrypt} = use('App/Helper/EncryptionHelper')

const WalletTransfer = use('App/Models/WalletTransfer')

const randomNumber = require('random-number');
const randomNumberOptions = {
  min: 100000000
  , max: 999999999
  , integer: true
}

const environment = 'production'

class ERC20TokenController {

  async getLenderInfo({response, auth, params}) {
    const lender = await UserPrivilege.findBy({privilegeGroupId: 4})
    if (!lender) return response.notFound({message: "Provider is not ready"})


    const masterAccount = await User.find(lender.user_id)
    const {address, mnemonic} = await masterAccount.wallet().fetch()


    const decryptedmnemonic = await decrypt(mnemonic)
    const web3 = ethConnector(decryptedmnemonic)

    const myBalanceWei = web3.utils.fromWei(await web3.eth.getBalance(address), 'ether')
    const lenderInfo = Object.assign(masterAccount, {address}, {balance: myBalanceWei, symbol: "eth"})

    return lenderInfo
  }

  async getTokenInfo({response, auth, params}) {

    const {mnemonic, privateKey, publicKey, address} = await auth.user.wallet().fetch()
    const StrxCoin = await Coins.findBy({symbol: params.symbol})

    if (!StrxCoin) return response.notFound({message: "Token's data not found"})
    if (StrxCoin.environment !== environment) return response.forbidden({message: "Token only on beta environment"})
    if (StrxCoin.verified === 0) return response.forbidden({message: "Token is not verified yet"})

    const gasPrice = await PaymentConfig.findBy({key: "TRANSACTION_GAS_PRICE"})
    const decryptedMnemonic = await decrypt(mnemonic)

    const contract = contractConnector(decryptedMnemonic, StrxCoin.address, gasPrice.value)

    const name = await contract.methods.name().call()
    const symbol = await contract.methods.symbol().call()
    const totalSupply = await contract.methods.totalSupply().call() / (10 ** await contract.methods.decimals().call())
    return response.ok({name, symbol, totalSupply, 'contractAddress': StrxCoin.address})
  }

  async getUserBalance({request, response, auth, params}) {
    const {mnemonic, address} = await auth.user.wallet().fetch()

    const StrxCoin = await Coins.findBy({symbol: params.symbol})
    if (!StrxCoin) return response.notFound({message: "Token's data not found"})
    if (StrxCoin.environment !== environment) return response.forbidden({message: "Token only on beta environment"})
    if (StrxCoin.verified === 0) return response.forbidden({message: "Token is not verified yet"})

    const gasPrice = await PaymentConfig.findBy({key: "TRANSFER_GAS_PRICE"})

    const encryptedMnemonic = await decrypt(mnemonic)
    const contractAddress = StrxCoin.address

    const contract = contractConnector(encryptedMnemonic, contractAddress, gasPrice.value)

    const balancePlain = await contract.methods.balanceOf(address).call()
    const decimals = await contract.methods.decimals().call()
    const balance = balancePlain / (10 ** decimals)
    return response.ok({total: balance})
  }

  async approvePayment({request, response, auth, params}) {
    const StrxCoin = await Coins.findBy({coin_symbol: STX})
    if (!StrxCoin) return response.notFound({message: "Token's data not found"})
    if (StrxCoin.environment !== environment) return response.forbidden({message: "Token only on beta environment"})
    if (StrxCoin.verified === 0) return response.forbidden({message: "Token is not verified yet"})

    const {mnemonic, address} = await auth.user.wallet().fetch()
    const {data} = params

    const order = await Order.findBy({data});
    if (!order) return response.badRequest({message: "No Order"})

    const merchant = await order.merchant().fetch()
    const merchatWallet = await merchant.wallet().fetch()

    if (address !== order.user_address) return response.badRequest({message: "This is not your order ETH20Controller.Approve"})


    const gasPrice = await PaymentConfig.findBy({key: "TRANSACTION_GAS_PRICE"})
    const encryptedMnemonic = await decrypt(mnemonic)
    const contractAddress = await decrypt(StrxCoin.contractAddress)
    const contract = contractConnector(encryptedMnemonic, contractAddress, gasPrice.value)
    const amountWei = (order.amount) * (10 ** 18)

    if (order.status !== 'pending') return response.badRequest({message: "Your order is already paid"})
    if (address === merchatWallet.address) return response.badRequest({message: "Receiver and Sender must not be same"})

    order.status = 'executed'
    await order.save()


    contract.methods
      .send(merchatWallet.address, new BigNumber(amountWei).toFixed(), order.data)
      .send({from: address})
      .on('error', async function (error) {
        console.log({tx: order.data, status: "failed"})

        order.status = 'failed'
        await order.save()
      })
      .on('receipt', async function (receipt) {


        const {events, transactionHash} = receipt
        const {Sent} = events
        const {returnValues} = Sent


        const {operator, from, to, amount, data} = returnValues

        console.log({
          key: data,
          status: 'approved'
        })


        order.status = 'approved'
        await order.save()

        await History.create({order_id: order.id, txHash: transactionHash, data: JSON.stringify(receipt)})
      })

    return response.ok({message: "executed"})
  }

  async transferToken({request, response, auth, params}) {
    const StrxCoin = await Coins.findBy({symbol: params.symbol})
    if (!StrxCoin) return response.notFound({message: "Token's data not found"})
    if (StrxCoin.environment !== environment) return response.forbidden({message: "Token only on beta environment"})

    const lender = await UserPrivilege.findBy({privilegeGroupId: 4})
    if (!lender) return response.notFound({message: "Provider is not ready"})


    const masterAccount = await User.find(lender.user_id)
    const masterAccountWallet = await masterAccount.wallet().fetch()


    const {amount, to} = request.all()

    const {id, mnemonic, publicKey, privateKey, address} = await auth.user.wallet().fetch()

    const gasPrice = await PaymentConfig.findBy({key: "TRANSFER_GAS_PRICE"})
    const fee = await PaymentConfig.findBy({key: "TRANSFER_FEE_STX"})

    const encryptedMnemonic = await decrypt(mnemonic)
    const contractAddress = StrxCoin.address
    const masterLenderMnemonic = await decrypt(masterAccountWallet.mnemonic)

    const contract = contractConnector(encryptedMnemonic, contractAddress, gasPrice.value)
    const web3 = ethConnector(masterLenderMnemonic)

    const decimals = 18;
    const amountWei = amount * (10 ** decimals)
    const reducementFee = fee.value * (10 ** decimals)

    const finalAmount = new BigNumber((amountWei - reducementFee).toString())
    const finalReducement = new BigNumber(reducementFee.toString())

    const randomId = await randomNumber(randomNumberOptions)
    const balancePlain = await contract.methods.balanceOf(address).call()

    if (address === to) return response.badRequest({message: "Receiver and Sender must not be same"})
    if (amountWei < 0) return response.badRequest({message: "Transaction Failed, minimum transaction is " + (transferFee.value * 10)})
    if (balancePlain < amountWei) return response.forbidden({message: "Insufficient Balance "})
    if (amountWei - reducementFee < reducementFee) return response.badRequest({message: "Transaction must be " + (fee.value * 2).toString()})

    const transferGas = await contract.methods.transfer(to, finalAmount.toFixed())
      .estimateGas({from: address}, async function (error, gasAmount) {
      })

    const feeGas = await contract.methods.transfer(masterAccountWallet.address, finalReducement.toFixed())
      .estimateGas({from: address}, async function (error, gasAmount) {
      })

    const pureTransferGasWei = parseFloat(gasPrice.value) * transferGas
    const feeTransferGasWei = parseFloat(gasPrice.value) * feeGas

    const transfer = await WalletTransfer.create({
      transaction_number: randomId,
      user_id: auth.user.id,
      token_id: StrxCoin.id,
      to,
      amountWei: (amountWei - reducementFee),
      amountFee: reducementFee,
      weiGas: (pureTransferGasWei + feeTransferGasWei),
      status: 'executed',
      environment
    })


    await web3.eth.sendTransaction({
      from: masterAccountWallet.address,
      to: address,
      value: (pureTransferGasWei + feeTransferGasWei).toString()
    })
      .on('error', async function (error) {
        console.log({error})
        return response.badRequest(error)
      })
      .on('receipt', async function (receipt) {
        console.log({sendEth: receipt.transactionHash})
      })

    contract.methods.transfer(to, finalAmount.toFixed()).send({from: address})
      .on('error', async function (error) {
        console.log({error})
        transfer.status = 'failed'
        await transfer.save()

      })
      .on('receipt', async function (receipt) {
        console.log({transferSTX: receipt.transactionHash})
        const {transactionHash} = receipt

        transfer.status = 'success'
        transfer.transactionHash = transactionHash
        transfer.receipt = JSON.stringify(receipt)
        await transfer.save()
      })

    contract.methods.transfer(masterAccountWallet.address, finalReducement.toFixed()).send({from: address})
      .on('error', async function (error) {
        console.log({error})
        transferFee.status = 'failed'
        await transferFee.save()
      })
      .on('receipt', async function (receipt) {
        console.log({paySTX: receipt.transactionHash})
      })

    return response.ok(transfer)


  }

  async getClientBalance({request, response, auth, params}) {
    const {mnemonic, address} = await Wallet.findBy({address: params.address})

    const StrxCoin = await Coins.findBy({coin_symbol: "stx"})

    const gasPrice = await PaymentConfig.findBy({key: "TRANSFER_GAS_PRICE"})
    const encryptedMnemonic = await decrypt(mnemonic)

    const contract = contractConnector(encryptedMnemonic, await decrypt(StrxCoin.contractAddress), gasPrice.value)

    const balancePlain = await contract.methods.balanceOf(address).call()
    const decimals = await contract.methods.decimals().call()
    const balance = balancePlain / (10 ** decimals)
    return response.ok({stx: balance})
  }


}

module.exports = ERC20TokenController
