'use strict'

const {Wallet} = use('ethers')
const User = use('App/Models/Credential/MasterUser')

const MasterWallet = use('App/Models/MasterEthereumWallet')
const {encrypt, decrypt, hash, verify} = use('App/Helper/EncryptionHelper')
const ActiveDevices = use('App/Models/Credential/SystemUserActiveDevice')
const Kyc = use('App/Models/LedgerAccountKyc')


class UserController {

  async login({request, response, auth}) {
    const {email, password} = request.all()
    const userAgent = request.header("user-agent")

    const isUserExist = await User.findBy({email: email})
    if (!isUserExist) return response.forbidden({message:"User Not Found"})

    const privileges = await isUserExist.privilege().fetch()
    const privilegesId = privileges.toJSON().map(data => data.privilegeGroupId)
    if (privilegesId.includes(6)) return response.forbidden({message: "Only real account"})


    const isSame = await verify(password, isUserExist.password)
    if (!isSame) return response.forbidden({message: "Password is not same",})

    const authToken = await auth.withRefreshToken().attempt(email, password)

    ActiveDevices.create({
      master_user_id: isUserExist.id,
      user_agent: userAgent,
      signin_time: new Date().toISOString().slice(0, 19).replace('T', ' '),
      ecosystem: "wallet",
      refresh_token: authToken.refreshToken,
      is_active: true,
    })


    return response.accepted(authToken)
  }

  async refreshToken({request,result,auth}){
    const {refreshToken} = request.all()
    const refresh = await auth.generateForRefreshToken(refreshToken)
    return refresh
  }

  async logout({request, response, auth}) {

    const {refreshToken} = request.all()
    const userAgent = request.header("user-agent")

    const currentAgent = await ActiveDevices.findBy({refresh_token: refreshToken})
    if (!currentAgent) return response.forbidden({message: "Refresh auth token not found"})

    currentAgent.is_active = false
    currentAgent.signout_time = new Date().toISOString().slice(0, 19).replace('T', ' ')


    await currentAgent.save()

    await auth.check();
    return await auth.revokeTokens([refreshToken], true)
  }

  async info({request, response, auth}) {


    const {mnemonic, privateKey, publicKey, address} = await auth.user.wallet().fetch()

    const userKyc = await Kyc.findBy({user_id:auth.user.id})
    if(!userKyc) return response.badRequest({message:"KYC is not complete"})
    return response.ok({
      user: auth.user,
      address,
      first_name:userKyc.first_name,
      last_name:userKyc.last_name,
      middle_name:userKyc.middle_name
    })
  }


}

module.exports = UserController

