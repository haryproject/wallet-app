'use strict'

const {ethConnector} = use('App/Helper/EthereumHelper')

const Coins = use('App/Models/ListingToken')
const PaymentConfig = use('App/Models/PaymentConfig')
const User = use('App/Models/Credential/MasterUser')

const WalletTransfer = use('App/Models/WalletTransfer')

const randomNumber = require('random-number');
const randomNumberOptions = {
  min: 100000000
  , max: 999999999
  , integer: true
}


const {encrypt, decrypt} = use('App/Helper/EncryptionHelper')

class EthController {

  async getUserBalance({request, response, auth}) {
    const {mnemonic, address} = await auth.user.wallet().fetch()

    const decryptedMnemonic = await decrypt(mnemonic)
    const web3 = ethConnector(decryptedMnemonic)
    const myBalance = web3.utils.fromWei(await web3.eth.getBalance(address), "ether")
    return response.ok({total: myBalance})
  }

  async transferEthereum({request, response, auth}) {
    const randomId = await randomNumber(randomNumberOptions)
    const {id, mnemonic, address} = await auth.user.wallet().fetch()

    const {amount, to} = request.all()

    const decryptedmnemonic = await decrypt(mnemonic)
    const web3 = ethConnector(decryptedmnemonic)
    const myBalance = web3.utils.fromWei(await web3.eth.getBalance(address), "ether")

    const myBalanceWei = web3.utils.fromWei(await web3.eth.getBalance(address), 'ether')
    if (myBalanceWei < amount) return response.badRequest({message:"Insufficient Balance"})

    const transfer = await WalletTransfer.create({
      transaction_number: randomId,
      user_id: auth.user.id,
      symbol:'eth',
      to,
      amountWei: web3.utils.toWei(amount, 'ether'),
      status: 'executed'
    })

    web3.eth.sendTransaction({
      from: address,
      to: to,
      value: web3.utils.toWei(amount, 'ether')
    }).on('error', async function (error) {
      console.log({error:error})
      transfer.status = 'failed'
      await transfer.save()
      return response.badRequest(error)
    })
      .on('receipt', async function (receipt) {
        console.log({transferETH: receipt.transactionHash})
        const {transactionHash} = receipt
        transfer.transactionHash =transactionHash
        transfer.status = 'success'
        transfer.receipt = JSON.stringify(receipt)
        await transfer.save()

      })


    return response.ok({message: "Executed"})
  }

  async history({request, response, auth, params}) {
    const StrxCoin = await Coins.findBy({coin_symbol: "eth"})


    const {id, mnemonic,} = await auth.user.wallet().fetch()
    const txHashs = await TransactHistory.all()

    const encryptedMnemonic = await decrypt(mnemonic)

    const txHash = txHashs.toJSON().filter(tx => tx.walletId === id)
    const web3 = ethConnector(encryptedMnemonic)


    let txHashList = []

    for (let i = 0; i < txHash.length; i++) {
      const tx = txHash[i]
      let transact = await web3.eth.getTransaction(tx.txHash)
      transact.value = web3.utils.fromWei(transact.value, 'ether')
      transact.symbol = StrxCoin.coin_symbol
      await txHashList.push(transact)
    }

    return response.ok(txHashList)
  }

  async detailHistory({request, response, auth, params}) {

    const StrxCoin = await Coins.findBy({coin_symbol: "eth"})
    const {tx} = params

    const {mnemonic,} = await auth.user.wallet().fetch()

    const encryptedMnemonic = await decrypt(mnemonic)
    const web3 = ethConnector(encryptedMnemonic)

    const transaction = await web3.eth.getTransaction(tx)
    transaction.value = web3.utils.fromWei(transaction.value, 'ether')
    transaction.symbol = StrxCoin.coin_symbol
    return response.ok(transaction)
  }

  async sendEthForTransaction({request, response, params}) {
    const {key} = params
    const {recipient, orderData, orderId} = request.all()

    const merchant = await PaymentMerchant.findBy({key})
    const masterLender = await User.find(merchant.lenderId)

    if (!merchant) return response.badRequest({message: "Not authorized merchant"})


    const {id, mnemonic, address} = await masterLender.wallet().fetch()
    const gasETH = await PaymentConfig.findBy({key: 'GAS_ETH'})

    const decryptedmnemonic = await decrypt(mnemonic)
    const web3 = ethConnector(decryptedmnemonic)

    const transaction = await web3.eth.sendTransaction({

      from: address,
      to: recipient,
      value: web3.utils.toWei(gasETH.value.toString(), 'ether')

    }).on('error', async function (error) {
      console.log(error)
    }).on('receipt', async function (receipt) {

      await GasTransaction.create({
        order_id: orderId,
        merchant_id: merchant.id,
        value: gasETH.value.toString(),
        receipt: JSON.stringify(receipt)
      })
    })


    return response.ok({txHash: transaction.transactionHash, merchant: key, recipient, gas: gasETH.value + " ETH"})


  }
}

module.exports = EthController
