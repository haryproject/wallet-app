'use strict'
const Contact = use('App/Models/WalletContact')


class WalletContactController {

  async createContract({request, response, auth}) {
    const {name, address} = request.all()
    const userId = auth.user.id
    return await Contact.create({user_id: userId, name, address})
  }


  async showContract({request, response, auth}) {
    return await Contact.query().where('user_id', auth.user.id).fetch()
  }

  async updateContract({request, response, auth, params}) {
    const {id} = params
    const {name, address} = request.all()

    const alreadyContact = await Contact.findBy({id})
    if (!alreadyContact) return response.noContent({message: "Contact data not registered"})
    if (alreadyContact.user_id !== auth.user.id) return response.badRequest({message: "Is not your contract"})


    alreadyContact.name = name
    alreadyContact.address = address

    await alreadyContact.save()
    return alreadyContact
  }

  async deleteContract({request, response, auth, params}) {
    const {id} = params

    const alreadyContact = await Contact.findBy({id})
    if (!alreadyContact) return response.noContent({message: "Contact data not registered"})
    if (alreadyContact.user_id !== auth.user.id) return response.badRequest({message: "Is not your contract"})

    await alreadyContact.delete()
    return alreadyContact
  }

}

module.exports = WalletContactController
