'use strict'
const {increase, decrease} = use('App/Helper/PointHelper')
const WalletConfiguration = use('App/Models/WalletConfig')
const Point = use('App/Models/WalletPoint')


class WalletPointController {


  async pointBalance({request, response, auth, params}) {
    const existingPoint = await WalletConfiguration.findBy({type:'point', mandatory: '1'})
    const {environment} = params
    const {address} = await auth.user.wallet().fetch()
    const point =  await Point.findBy({address,environment})
    if(!point) return {symbol:existingPoint.symbol,balance:0}

    const balance = point.point / (10**existingPoint.decimals)
    return {symbol:existingPoint.symbol,balance:balance,address}
  }

  async increasePoint({request, response, auth, params}) {
    const existingToken = await WalletConfiguration.findBy({type: 'point', mandatory: '1'})
    const {environment} = params
    const {point} = request.all()
    const poinRaw = point * (10 ** existingToken.decimals)
    const {address} = await auth.user.wallet().fetch()

    const currentBalance = await increase(address, poinRaw, environment)
    const balance = currentBalance.point / (10 ** existingToken.decimals)
    return {symbol: existingToken.symbol, balance}
  }

  async decreasePoint({request, response, auth, params}) {
    const existingToken = await WalletConfiguration.findBy({type: 'point', mandatory: '1'})
    const {environment} = params
    const {point} = request.all()
    const poinRaw = point * (10 ** existingToken.decimals)
    const {address} = await auth.user.wallet().fetch()

    const currentBalance = await decrease(address, poinRaw, environment)
    const balance = currentBalance.point / (10 ** existingToken.decimals)
    return {symbol: existingToken.symbol, balance}
  }
}

module.exports = WalletPointController
