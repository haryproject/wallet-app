'use strict'
const Encryption = use('Encryption')
const Hash = use('Hash')
const {sendForgotPinConfirmation} = use('App/Helper/EmailHelper.js')
const User = use('App/Models/Credential/MasterUser')
const Wallet = use('App/Models/WalletEthereum')
const randomNumber = require('random-number');
const randomNumberOptions = {min: 100000, max: 999999, integer: true}


class WalletController {


  async sendForgotPin({request, response, auth}) {
    const client = auth.user
    if (!client) return response.notFound({message: "User not found"})
    const clientWallet = await auth.user.wallet().fetch()


    const randomCode = await randomNumber(randomNumberOptions)
    clientWallet.unique = randomCode
    await clientWallet.save()

    const {email} = client
    const sendEmailReceipt = await sendForgotPinConfirmation(randomCode, email, "Forgot Pin")

    return {receipt: sendEmailReceipt.messageId}
  }

  async changePin({request, response, auth}) {
    const {key, pin} = request.all()

    const activeUserWallet = await Wallet.findBy({unique: key})
    if (!activeUserWallet) return response.forbidden({message: "Invalid key"})

    const timeDifferent = (Date.now() - activeUserWallet.updated_at);
    const minuteDif = Math.round((timeDifferent / 1000) / 60);
    const isAlreadyExpired = minuteDif > 1

    if (isAlreadyExpired) return response.forbidden({message: "Key already expired"})

    activeUserWallet.pin = pin
    activeUserWallet.unique = ""
    await activeUserWallet.save()

    return {success: true}
  }

  async createPin({request, response, auth}) {
    const activeUserWallet = await auth.user.wallet().fetch()
    if (!activeUserWallet) return response.forbidden({message: "Invalid Wallet"})

    if (activeUserWallet.pin) return response.forbidden({message: "Wallet already has a pin"})

    const {pin} = request.all()
    activeUserWallet.pin = pin
    await activeUserWallet.save()

    return {success: true}
  }

  async verifyPin({request, response, auth}) {
    const wallet = await auth.user.wallet().fetch()
    const {pin} = request.all()
    const isPinCorrect = await Hash.verify(pin, wallet.pin)
    return {verified: isPinCorrect}
  }

}

module.exports = WalletController
