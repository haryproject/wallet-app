'use strict'
const WalletConfiguration = use('App/Models/WalletConfig')
const {ethereum, contract} = use('App/Helper/EnvironmentHelper')
const {decrypt} = use('App/Helper/EncryptionHelper')
const WalletTransaction = use('App/Models/WalletTransfer')
const {lendEth} = require('starx-lender-client')
const Fee = use('App/Models/WalletFeeStructure')
const uuid = require('uuid/v4')
const Order = use('App/Models/PaymentOrder')
const Merchant = use('App/Models/PaymentMerchant')
const {getUserHistory} = use('App/Helper/TransferHistoryHelper')
const Transaction = use('App/Models/LedgerAccountTransaction')
const PaymentSession = use('App/Models/PaymentOpenSession')
const Kyc = use("App/Models/LedgerAccountKyc")
const Env = use('Env')


const {sendPaymentSuccessConfirmation, sendPaymentFailedConfirmation, sendTransferSuccessConfirmation, sendTransferFailedConfirmation,} = use('App/Helper/EmailHelper')


class WalletTokenController {


  async clientTransferHistory({request, response, auth, params}) {
    const {year, month, date} = params
    const orderHistory = await getUserHistory(auth.user.id, year, month, date)
    return orderHistory[0]
  }

  async clientTransferHistoryYear({request, response, auth, params}) {
    const {year} = params
    const orderHistory = await getUserHistory(auth.user.id, year)
    return orderHistory[0]
  }

  async clientTransferHistoryMonth({request, response, auth, params}) {
    const {year, month} = params
    const orderHistory = await getUserHistory(auth.user.id, year, month)
    return orderHistory[0]
  }

  async clientTransferHistoryDay({request, response, auth, params}) {
    const {year, month, date} = params
    const orderHistory = await getUserHistory(auth.user.id, year, month, date)
    return orderHistory[0]
  }


  async tokenBalance({request, response, auth, params}) {
    const {environment} = params

    const {mnemonic, address} = await auth.user.wallet().fetch()
    const decryptedMnemonic = await decrypt(mnemonic)

    const token = await WalletConfiguration.findBy({type: 'token', mandatory: '1'})
    const tokenAddress = environment === 'live' ? token.live_address : token.local_address
    const ethereumUrl = environment === 'live' ? token.live_network_url : token.local_network_url


    const smartContract = await contract(environment, ethereumUrl, decryptedMnemonic, tokenAddress, 0)
    const balanceWei = await smartContract.methods.balanceOf(address).call()

    const balance = balanceWei / (10 ** token.decimals)
    return {balance, symbol: token.symbol, address,}
  }

  async previewTransaction({request, response, auth, params}) {
    const {environment} = params
    const token = await WalletConfiguration.findBy({type: 'token', mandatory: '1'})
    const tokenAddress = environment === 'live' ? token.live_address : token.local_address

    const {to, amount} = request.all()

    const fee = await Fee.query().where('amount', '<=', amount).orderBy('amount', 'desc').first()
    if (!fee) return response.notFound({message: "Fee Structure not found"})
    const {tax, point_commision} = fee

    const taxCut = parseFloat((amount * tax).toFixed(4))
    const cleanAmount = parseFloat((amount * (1 + tax)).toFixed(4))
    const pointCommision = cleanAmount * point_commision

    return {
      receiver: to,
      token: amount,
      tokenSymbol: token.symbol,
      taxAmount: taxCut,
      cleanAmount,
    }
  }

  async sendToken({request, response, auth, params}) {

    const {environment} = params
    const {to, amount, note} = request.all()
    const {mnemonic, address} = await auth.user.wallet().fetch()
    const decryptedPrivateKey = await decrypt(mnemonic)

    const userData = await Kyc.findBy({user_id: auth.user.id})

    if (to === address) return response.badRequest({message: "Receiver and sender must not be same"})

    const fee = await Fee.query().where('amount', '<=', amount).orderBy('amount', 'desc').first()
    if (!fee) return response.notFound({message: "Fee Structure not found"})

    const {tax, point_commision, gasprice} = fee
    const taxCut = parseFloat((amount * tax).toFixed(4))
    const cleanAmount = parseFloat((amount * (1 + tax)).toFixed(4))

    const token = await WalletConfiguration.findBy({type: 'token', mandatory: '1'})
    const tokenAddress = environment === 'live' ? token.live_address : token.local_address
    const ethereumUrl = environment === 'live' ? token.live_network_url : token.local_network_url

    const smartContract = await contract(environment, ethereumUrl, decryptedPrivateKey, tokenAddress, gasprice)
    const web3 = await ethereum(environment, ethereumUrl, decryptedPrivateKey)
    const taxAmountWei = taxCut * (10 ** token.decimals)
    const cleanAmountWei = amount * (10 ** token.decimals)

    const balanceWei = await smartContract.methods.balanceOf(address).call()
    const balance = parseFloat((balanceWei / (10 ** token.decimals)).toFixed(4))

    const isValidAddress = await web3.utils.isAddress(to);
    if (!isValidAddress) return response.badRequest({message: "Receiver address invalid"})
    if (cleanAmount > balance) return response.badRequest({message: "Insufficient balance"})

    const transferGas = await smartContract.methods.transfer(to, cleanAmountWei.toString()).estimateGas({from: address}, async function (error, gasAmount) {
    })

    const sendLenderGas = await smartContract.methods.transfer(to, taxAmountWei.toString()).estimateGas({from: address}, async function (error, gasAmount) {
    })

    const lendTransferEthAmount = await web3.utils.fromWei(((transferGas * gasprice) + (sendLenderGas * gasprice)).toString(), 'ether')

    const config = {
      host: Env.getOrFail('ETH_SERVER'),
      uuid: token.lender_uuid,
      receiver: address,
      amount: lendTransferEthAmount,
      environment,
      configId: token.id
    }


    console.log(await lendEth(config))


    const transaction_unique = uuid()

    const ledgerTransfer = await Transaction.create({
      transaction_no: transaction_unique,
      sender: address,
      receiver: to,
      amount,
      source: 'token',
      type: 'transfer'
    })

    const transferHistory = await WalletTransaction.create({
      transaction_number: ledgerTransfer.transaction_no,
      source: 'token',
      user_id: auth.user.id,
      amount,
      cleanAmount,
      to,
      taxAmount: taxCut,
      status: 'executed',
      note
    })

    const user = userData.first_name + " " + userData.last_name

    await smartContract.methods.transfer(to, cleanAmountWei.toString()).send({from: address})
      .on('error', async function (error) {
        console.log(error)
        transferHistory.status = 'failed'
        await transferHistory.save()
        console.log(await sendTransferFailedConfirmation(transferHistory.amount, token.symbol, to, user, auth.user.email, "Transfer Failed",))

      })
      .on('receipt', async function (receipt) {
        console.log({transferSTX: receipt.transactionHash})
        const {transactionHash} = receipt

        transferHistory.status = 'success'
        transferHistory.transactionHash = transactionHash
        transferHistory.receipt = JSON.stringify(receipt)
        await transferHistory.save()

        await sendTransferSuccessConfirmation(transferHistory.amount, token.symbol, transferHistory.taxAmount, to, user, auth.user.email, "Transfer Success",)

        smartContract.methods.transfer(token.lender_address, taxAmountWei.toString()).send({from: address})
          .on('error', async function (error) {
            console.log({error})
          })
          .on('receipt', async function (receipt) {
            console.log({transferTaxSTX: receipt.transactionHash})
          })

      })


    return transferHistory
  }

  // async previewOrder({request, response, auth, params}) {
  //   const {address} = await auth.user.wallet().fetch()
  //
  //   const {order_code} = params
  //   const order = await Order.findBy({order_code})
  //   if (!order) return response.notFound({message: "Order not found"})
  //   const {source, amount, cleanAmount, taxAmount, status, payer_address, pointReward} = order
  //
  //   const merchant = await Merchant.find(order.merchant_id)
  //   if (!merchant) return response.notFound({message: "Merchant not found"})
  //   const {name, email, phone, lender_uuid} = merchant
  //
  //   return {
  //     name,
  //     email,
  //     phone,
  //     lender_uuid,
  //     source,
  //     amount,
  //     cleanAmount,
  //     taxAmount,
  //     pointReward,
  //     status,
  //   }
  // }
  //
  // async payOrder({request, response, auth, params}) {
  //   const client = await auth.user
  //   const {address, mnemonic} = await auth.user.wallet().fetch()
  //   const decryptedPrivateKey = await decrypt(mnemonic)
  //
  //   const {order_code, environment} = params
  //   const order = await Order.findBy({order_code})
  //   if (!order) return response.notFound({message: "Order not found"})
  //   if (order.status === 'approved' || order.status === 'cancelled') return response.badRequest({message: "Order not available"})
  //
  //   const {source, amount, cleanAmount, taxAmount, status, payer_address, pointReward} = order
  //
  //   const merchant = await Merchant.find(order.merchant_id)
  //   if (!merchant) return response.notFound({message: "Merchant not found"})
  //   const {name, email, phone, lender_uuid} = merchant
  //
  //   if (source !== 'token') return response.forbidden({message: "Pay using point instead"})
  //   const token = await WalletConfiguration.findBy({type: 'token', mandatory: '1'})
  //   const tokenAddress = environment === 'live' ? token.live_address : token.local_address
  //
  //
  //   const smartContract = await contract(environment, decryptedPrivateKey, tokenAddress, token.gas_price)
  //   const web3 = await ethereum(environment, decryptedPrivateKey)
  //   const cleanAmountWei = cleanAmount * (10 ** token.decimals)
  //
  //   const balanceWei = await smartContract.methods.balanceOf(address).call()
  //   const balance = parseFloat((balanceWei / (10 ** token.decimals)).toFixed(4))
  //
  //   if (cleanAmount > balance) return response.badRequest({message: "Insufficient balance"})
  //
  //   const transferGas = await smartContract.methods.transfer(merchant.walletAddress, cleanAmountWei.toString()).estimateGas({from: address}, async function (error, gasAmount) {
  //   })
  //
  //   const lendTransferEthAmount = await web3.utils.fromWei((transferGas * token.gas_price).toString(), 'ether')
  //
  //   const config = {
  //     host: Env.getOrFail('ETH_SERVER'),
  //     uuid: token.lender_uuid,
  //     receiver: address,
  //     amount: lendTransferEthAmount,
  //     environment,
  //     configId: token.id
  //   }
  //
  //   const ledgerTransfer = await Transaction.create({
  //     transaction_no: order.order_code,
  //     sender: address,
  //     receiver: merchant.walletAddress,
  //     amount: order.amount,
  //     source: 'token',
  //     type: 'payment'
  //   })
  //
  //   await lendEth(config)
  //
  //
  //   order.status = 'executed'
  //   await order.save()
  //
  //   await smartContract.methods.transfer(merchant.walletAddress, cleanAmountWei.toString()).send({from: address})
  //     .on('error', async function (error) {
  //       console.log({error})
  //       order.status = 'failed'
  //       await order.save()
  //
  //       await sendPaymentFailedConfirmation(name, order_code, token.symbol, order.amount, order.taxAmount, error.toString(), client.email, "Payment Failed")
  //
  //
  //     })
  //     .on('receipt', async function (receipt) {
  //       console.log({transferSTX: receipt.transactionHash})
  //       const {transactionHash} = receipt
  //
  //       order.status = 'approved'
  //       order.transactionHash = transactionHash
  //       order.receipt = JSON.stringify(receipt)
  //       await order.save()
  //
  //       await sendPaymentSuccessConfirmation(name, order_code, token.symbol, order.amount, order.taxAmount, client.email, "Payment Success")
  //     })
  //
  //
  //   return order
  //
  //
  // }

  async getTokenChart({request, response, auth, params}) {

  }
}

module.exports = WalletTokenController
