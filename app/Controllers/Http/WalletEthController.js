'use strict'
const WalletConfiguration = use('App/Models/WalletConfig')
const {ethereum, contract} = use('App/Helper/EnvironmentHelper')
const {encrypt, decrypt} = use('App/Helper/EncryptionHelper')
const Point = use('App/Models/WalletPoint')


class WalletTokenController {

  async ethBalance({request, response, auth, params}) {
    const {environment} = params
    const {mnemonic,address} = await auth.user.wallet().fetch()

    const token = await WalletConfiguration.findBy({type: 'token', mandatory: '1'})


    const decryptedMnemonic = await decrypt(mnemonic)
    const ethereumUrl = environment === 'live'? token.live_network_url:token.local_network_url

    const web3 = await ethereum(environment,ethereumUrl, decryptedMnemonic)
    const balance = web3.utils.fromWei(await web3.eth.getBalance(address),'ether')
    return {balance, symbol: 'eth'}
  }






}

module.exports = WalletTokenController
