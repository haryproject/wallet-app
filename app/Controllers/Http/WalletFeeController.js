'use strict'
const FeeStructure = use('App/Models/WalletDepositFeeStructure')

class WalletFeeController {


    async index(){
        return await FeeStructure.all()
    }

    async store({ request, response, auth }) {
        const { threshold, amount, percentage } = request.all()
        const currency = "usd"
        return await FeeStructure.create({ threshold, amount, percentage, currency })
    }

    async update({request,response,auth,params}){
        const {id} = params
        const { threshold, amount, percentage } = request.all()
        const currency = "usd"
        const feeStructure = await FeeStructure.find(id)
        if(!feeStructure) return response.notFound({message:"Fee Structure not found"})
        
        feeStructure.threshold = threshold
        feeStructure.amount = amount
        feeStructure.percentage = percentage

        await feeStructure.save()
        
        return feeStructure

    }

    async delete({request,response,auth,params}){
        const {id} = params
        const feeStructure = await FeeStructure.find(id)

        if(!feeStructure) return response.notFound({message:"Fee structure nor found"})
        const deleted = await feeStructure.delete()

        if(!deleted) return response.badReuest({message:"Internal server error"})
        return {success:true}
    }

}

module.exports = WalletFeeController
