'use strict'
const WalletConfiguration = use('App/Models/WalletConfig')


class WalletConfigController {

  async modifyToken({request, response, auth, params}) {
    const {environment} = params
    const type = 'token'
    const mandatory = true
    const {symbol, name, live_address, local_address, decimals,gas_price,lender_address,lender_uuid} = request.all()

    const existingToken = await WalletConfiguration.findBy({type: 'token', mandatory: '1'})
    if (!existingToken) return await WalletConfiguration.create({
      symbol,
      name,
      live_address,
      local_address,
      decimals,
      type,
      mandatory,
      gas_price,
      lender_uuid,
      lender_address
    })

    existingToken.name = name
    existingToken.symbol = symbol
    existingToken.live_address = live_address
    existingToken.local_address = local_address
    existingToken.decimals = decimals
    existingToken.gas_price = gas_price
    existingToken.lender_uuid = lender_uuid
    existingToken.lender_address = lender_address

    await existingToken.save()

    return existingToken


  }

  async modifyPoint({request, response, auth, params}) {
    const {environment} = params
    const type = 'point'
    const mandatory = true
    const {symbol, name, live_address,local_address, decimals,lender_address,gas_price,lender_uuid} = request.all()

    const existingToken = await WalletConfiguration.findBy({type, mandatory: '1'})
    if (!existingToken) return await WalletConfiguration.create({
      symbol,
      name,
      live_address,
      local_address,
      decimals,
      type,
      mandatory,
      gas_price,
      lender_uuid,
      lender_address  })

    existingToken.name = name
    existingToken.symbol = symbol
    existingToken.live_address = live_address
    existingToken.local_address = local_address
    existingToken.decimals = decimals
    existingToken.gas_price = gas_price
    existingToken.lender_uuid = lender_uuid
    existingToken.lender_address = lender_address

    await existingToken.save()

    return existingToken


  }

  async infoToken({request, response, auth, params}) {
    const type = 'token'
    const existingToken = await WalletConfiguration.findBy({type, mandatory: '1'})
    return existingToken


  }

  async infoPoint({request, response, auth, params}) {
    const type = 'point'
    const existingPoint = await WalletConfiguration.findBy({type, mandatory: '1'})
    return existingPoint


  }


}

module.exports = WalletConfigController
