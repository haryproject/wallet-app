'use strict'
const Fee = use('App/Models/WalletFeeStructure')

class FeeStructureController {
  async show({request,response,auth}){
    return await Fee.query().orderBy('amount','asc').fetch()
  }


  async store({request,response,auth}){
    const {amount,tax,point_commision,gasprice} = request.all()
    return await Fee.create({amount,tax,point_commision,gasprice})
  }



}

module.exports = FeeStructureController
