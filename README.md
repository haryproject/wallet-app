# Wallet Master API

Documentation for ledger apps in starx ecosystem.
Wallet App support Ethereum enviroment with ERC-20 Token Standar and it's Delegated (ERC-20).
This system is made to work in STARX GLOBAL ECOSYSTEM.
Some of the function that used in ecosystem used based on ERC-777 Standard.


## Setup

To setup the project please install all the the npm module

```bash
npm install
```


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```


### Encryption
Update the encryption endpoint on the App/Helper/EncryptionHelper.js the current Encryption provider

```js
module.exports.encrypt = async function (data) {
  let result;
  await axios.post('{{CURRENT_ENV_MASTER_ENDPOINT}}'+apiKey+'/encrypt', {
    data : data
  })
    .then(function (response) {
      result = response.data
      return response.data
    })
    .catch(function (error) {

      return false
    });
  // console.log(result)
  return  result
}
```

### Documentation
Use this following URL to get the documentation 


**User Registrtion**

`https://documenter.getpostman.com/view/7920071/SW14Uwo9`

**Production**

`https://documenter.getpostman.com/view/7920071/SW17SvMm`

**Sandbox**

`https://documenter.getpostman.com/view/7920071/SW17SvMn`
