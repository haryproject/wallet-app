'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WalletPointSchema extends Schema {
  up () {
    this.create('wallet_points', (table) => {
      table.increments()
      table.string('address')
      table.double('point',255).unsigned()
      table.string('environment')
      table.timestamps()
    })
  }

  down () {
    this.drop('wallet_points')
  }
}

module.exports = WalletPointSchema
