'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MasterWalletSchema extends Schema {
  up () {
    this.create('wallet_ethereums', (table) => {
      table.increments()
      table.integer('userId').unsigned().references('id').inTable('master_users')
      table.text('mnemonic')
      table.text('privateKey')
      table.text('publicKey')
      table.text('address')
      table.text('pin')
      table.timestamps()
    })
  }

  down () {
    this.drop('wallet_ethereums')
  }
}

module.exports = MasterWalletSchema
