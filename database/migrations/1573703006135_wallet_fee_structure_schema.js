'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WalletFeeStructureSchema extends Schema {
  up () {
    this.create('wallet_fee_structures', (table) => {
      table.increments()
      table.double('amount')
      table.double('tax')
      table.double('point_commision')
      table.double('gasprice')
      table.timestamps()
    })
  }

  down () {
    this.drop('wallet_fee_structures')
  }
}

module.exports = WalletFeeStructureSchema
