'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WalletTransferSchema extends Schema {
  up () {
    this.create('wallet_transfers', (table) => {
      table.increments()
      table.string('transaction_number')
      table.enu('source',['token','point']).default('token')
      table.integer('user_id').unsigned().references('id').inTable('master_users')
      table.string('to')
      table.double('amount')
      table.double('cleanAmount')
      table.double('taxAmount')
      table.text('note')
      table.string('transactionHash')
      table.text('receipt')
      table.enu('status',['executed','success','failed'])
      table.timestamps()
    })
  }

  down () {
    this.drop('wallet_transfers')
  }
}

module.exports = WalletTransferSchema
