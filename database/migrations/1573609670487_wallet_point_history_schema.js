'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WalletPointHistorySchema extends Schema {
  up () {
    this.create('wallet_point_histories', (table) => {
      table.increments()
      table.string('address')
      table.double('point')
      table.enu('side',['deposit','withdraw'])
      table.string('environment')
      table.timestamps()
    })
  }

  down () {
    this.drop('wallet_point_histories')
  }
}

module.exports = WalletPointHistorySchema
