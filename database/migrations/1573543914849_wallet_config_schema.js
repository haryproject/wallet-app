'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WalletConfigSchema extends Schema {
  up () {
    this.create('wallet_configs', (table) => {
      table.increments()
      table.string('live_address')
      table.string('local_address')
      table.string('name')
      table.string('symbol')
      table.double('decimals')
      table.enu('type',['token','point'])
      table.boolean('mandatory').default('false')
      table.string('gas_price')
      table.string('lender_uuid')
      table.string('lender_address')
      table.string('paypal_client_id')
      table.string('paypal_secret')
      table.text('live_network_url')
      table.text('live_websocket_url')
      table.text('local_network_url')
      table.text('local_websocket_url')
      table.timestamps()
    })
  }

  down () {
    this.drop('wallet_configs')
  }
}

module.exports = WalletConfigSchema
