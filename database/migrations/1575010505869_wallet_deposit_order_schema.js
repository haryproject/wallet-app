'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WalletDepositOrderSchema extends Schema {
  up() {
    this.create('wallet_deposit_orders', (table) => {
      table.increments()
      table.string('order_id')
      table.string('paypal_order_id')
      table.integer('user_id').unsigned().references('id').inTable('master_users')
      table.double('amount')
      table.double('token')
      table.double('tax')
      table.text('payment_link')
      table.enu('status',['pending','authorized','captured','failed'])
      table.timestamps()
    })
  }

  down() {
    this.drop('wallet_deposit_orders')
  }
}

module.exports = WalletDepositOrderSchema
