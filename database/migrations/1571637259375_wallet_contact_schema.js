'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WalletContactSchema extends Schema {
  up () {
    this.create('wallet_contacts', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('master_users')
      table.string('name')
      table.text('address')
      table.timestamps()
    })
  }

  down () {
    this.drop('wallet_contacts')
  }
}

module.exports = WalletContactSchema
