'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WalletDepositFeeStructureSchema extends Schema {
  up () {
    this.create('wallet_deposit_fee_structures', (table) => {
      table.increments()
      table.double('threshold')
      table.double('amount')
      table.double('percentage')
      table.enu('currency',['usd','idr'])
      table.timestamps()
    })
  }

  down () {
    this.drop('wallet_deposit_fee_structures')
  }
}

module.exports = WalletDepositFeeStructureSchema
