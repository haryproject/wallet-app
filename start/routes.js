'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/
const Encryption = use('Encryption')
/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return {greeting: 'Wallet Master API'}
})


Route.group(() => {

  Route.get('/lender', 'ERC20TokenController.getLenderInfo')
  Route.post('/token/modify','Admin/WalletConfigController.modifyToken').middleware('auth','admin').validator('WalletConfig')
  Route.post('/point/modify','Admin/WalletConfigController.modifyPoint').middleware('auth','admin').validator('WalletConfig')

  Route.get('/token','Admin/WalletConfigController.infoToken')
  Route.get('/point','Admin/WalletConfigController.infoPoint')

  Route.get('/fee','Admin/FeeStructureController.show')
  Route.post('/fee','Admin/FeeStructureController.store').validator('FeeStructure')

  Route.get('/eth/balance','WalletEthController.ethBalance').middleware(['auth','valid'])

  Route.get('/token/balance','WalletTokenController.tokenBalance').middleware(['auth','valid'])
  Route.get('/token/send','WalletTokenController.previewTransaction').middleware(['auth','valid']).validator('OverviewSendToken')
  Route.get('/token/pay/:order_code','WalletTokenController.previewOrder').middleware(['auth','valid'])
  Route.get('/token/transfer','WalletTokenController.clientTransferHistory').middleware(['auth','valid'])
  Route.get('/token/transfer/:year','WalletTokenController.clientTransferHistoryYear').middleware(['auth','valid'])
  Route.get('/token/transfer/:year/:month','WalletTokenController.clientTransferHistoryMonth').middleware(['auth','valid'])
  Route.get('/token/transfer/:year/:month/:date','WalletTokenController.clientTransferHistoryDay').middleware(['auth','valid'])

  Route.post('/token/send','WalletTokenController.sendToken').middleware(['auth','valid']).validator('SendToken')
  Route.post('/token/pay/:order_code','WalletTokenController.payOrder').middleware(['auth','valid'])

  Route.get('/point/balance','WalletPointController.pointBalance').middleware(['auth','valid'])
  Route.post('/point/increase','WalletPointController.increasePoint').middleware('auth','admin')
  Route.post('/point/decrease','WalletPointController.decreasePoint').middleware('auth','admin')

  Route.post('/pin/create','WalletController.createPin').middleware(['auth','valid']).validator('CreatePin')
  Route.post('/pin/forgot','WalletController.sendForgotPin').middleware(['auth','valid'])
  Route.post('/pin/change','WalletController.changePin').middleware(['auth','valid']).validator('ChangePin')
  Route.post('/pin/verify','WalletController.verifyPin').middleware(['auth','valid']).validator('VerifyPin')

  Route.get('/fee/structure', 'WalletFeeController.index')
  Route.post('/fee/structure', 'WalletFeeController.store').middleware('auth', 'admin').validator('StoreDepositFeeStructure')
  Route.put('/fee/structure/:id', 'WalletFeeController.update').middleware('auth', 'admin').validator('StoreDepositFeeStructure')
  Route.delete('/fee/structure/:id', 'WalletFeeController.delete').middleware('auth', 'admin')

  Route.get('/contact','WalletContactController.showContract').middleware(['auth','valid'])
  Route.post('/contact','WalletContactController.createContract').middleware(['auth','valid']).validator('StoreContact')
  Route.put('/contact/:id','WalletContactController.updateContract').middleware(['auth','valid']).validator('StoreContact')
  Route.delete('/contact/:id','WalletContactController.deleteContract').middleware(['auth','valid'])

  Route.get('/deposit/usd', 'WalletDepositController.preview').middleware('auth','valid','no_admin')
  Route.post('/deposit/usd', 'WalletDepositController.deposit').middleware('auth', 'valid','no_admin').validator('Deposit')
  Route.post('/deposit/verify','WalletDepositController.confirmDeposit').validator('VerifyDeposit')
  Route.post('/deposit/claim','WalletDepositController.claimMoney').middleware('auth','admin').validator('ClaimMoney')

}).prefix('/api/v1/wallet/:environment')


